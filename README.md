# Chicago Police Misconduct Processing Guide

The processing information and script is located inside of this directory [at this link](https://gitlab.com/JaredRutecki/chicago-police-misconduct-processing-guide/-/blob/main/CPD_Misconduct.RMD).

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/JaredRutecki/chicago-police-misconduct-processing-guide.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/JaredRutecki/chicago-police-misconduct-processing-guide/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Description
The RMD in this directory will go through the steps used to update the Chicago Police Misconduct Google Sheet and analyze the results in a uniform style. The process involves converting a PDF to a spreadsheet, cleaning the data so each officer identified in the Department of Law records is accounted for, binding that cleaned sheet to the old data, joining the new data to the old records on settlements/judgements by individual, and pulling court documents from PACER/RECAP and Cook County courts to help identify the individuals in the data where an ID is unclear while confirming matching names are the same individual. It has been [used previously in stories like this](https://news.wttw.com/2024/01/22/repeated-police-misconduct-141-officers-cost-chicago-taxpayers-1428m-over-4-years).


## Installation
This can be used as a guide to do data cleaning in Excel, or the script requires R Studio with the tidyverse package.


## Support
Reach out to Jared at the account connected to this directory.

## License
Copyright 2024 Jared Rutecki

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Project status
The data for 2023 has been updated online. This script should work next year, and will work with new data. My next steps are to identify all of the officers named in cases by badge number in the tab labeled "OfficerID". Then, I would like to create three tables for the database - one by case number with years and payment amounts, one with Officer ID information, and one with cases linked to officer IDs. Then a very lean series of smaller tables can be joined together, eliminating the need to use the wide Google Sheet we have currently. Also, this setup will allow the data to be joined to other projects by WTTW and other newsrooms.
