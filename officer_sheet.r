# Step 1: Load necessary libraries
library(dplyr)  # For data manipulation
library(tidyr)  # For reshaping data

# Step 2: Load the CSV files, filter blanks
cases <- read.csv("cases.csv")
officers <- read.csv("officer.csv")

# Step 3: Reshape the cases data (Q through AS are officer names)
# Gather officer names from columns Q to AS into a single column
cases_long <- cases %>%
  pivot_longer(cols = 17:35, names_to = "officer_column", values_to = "officer_name") %>%
  filter(!is.na(officer_name))  # Remove rows with no officer names

# Step 4: Join the reshaped cases data with the officers data
# Assuming the first column in officers.csv is the officer names, we join on that.
merged_data <- cases_long %>%
  left_join(officers, by = c("officer_name" = "officer"))  # Change 'Officer_Name' to the actual column name in officers.csv if needed

# Step 5: Select relevant columns (case name, case number, officer ID, badge number)
# Assuming case name and number are in the columns "case_name" and "case_number", and officer ID and badge number are in the officers.csv.
final_data <- merged_data %>%
  select(Case.Name, Case.Number, officer_ID, badge)

# Step 6: Write the result to a new CSV file
write.csv(final_data, "merged_cases_officers.csv", row.names = FALSE)